#!/usr/bin/env node

const [, , ...args] = process.argv;

const selectCommand = require("./selectCommand");

selectCommand(args)
  .then((r) => {
    console.log(r);
    process.exit(0);
  })
  .catch((e) => {
    console.error(e);
    process.exit(1);
  });
