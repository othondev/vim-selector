const vimManager = require("./vimManager");
const { createFolder } = require("./folderOperator");

createFolder();

module.exports = (args) => {
  const [command, ...params] = args;
  switch (command) {
    case "remove":
      return vimManager.removeRepository(params);
    case "update":
      return vimManager.updateRepository(params);
    case undefined:
    case "":
    case "ls":
    case "list":
      return vimManager.listRepository();
    default:
      return vimManager.addRepository(command)
        .then(url => vimManager.changeRepositoy(url));
  }
};
