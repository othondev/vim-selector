const { exec } = require("child_process");
const { PATH_REPOSITORY = `${process.env.HOME}/.vimManager` } = process.env;

async function sh(command) {
  return new Promise((resolve, reject) => {
    exec(command, (err, stdout, stderr) => {
      if (err) {
        reject(err);
      } else {
        resolve(stdout ? stdout.replace("\n", "") : stderr.replace("\n", ""));
      }
    });
  });
}

exports.clone = (url, folder) => {
  return sh(`(cd ${PATH_REPOSITORY} ; git clone ${url} ${folder})`);
};

exports.pull = (folder) => {
  return sh(`( cd ${PATH_REPOSITORY}/${folder}; git pull origin master )`);
};

exports.find = async (folder) => {
  const vimrcPath = await sh(`find ${PATH_REPOSITORY}/${folder} -name vimrc`);
  if (!vimrcPath)
    throw new Error(
      `No vimrc file found in ${PATH_REPOSITORY}/${folder} repository`
    );
  return vimrcPath;
};
