const {
  PATH_VIMRC = `${process.env.HOME}/.vimrc`,
} = process.env;
const fs = require("fs");
const { find } = require("./systemCall")

exports.addLineRepository = async (repositoryName) => {
  const exist = fs.existsSync(PATH_VIMRC);
  const vimrc = await find(repositoryName)
  const newLine = `source ${vimrc}`;
  let file;

  if (exist) {
    file = fs.readFileSync(PATH_VIMRC);
    const text = file.toString("utf8").replace(/source.*vimrc/g, "");
    fs.writeFileSync(PATH_VIMRC, text);
  }
  return fs.appendFileSync(PATH_VIMRC, newLine);
};
