const fs = require("fs");
const { promisify } = require("util");
const readdirPromise = promisify(fs.readdir);
const rmPromise = promisify(fs.rmdir);
const { PATH_REPOSITORY = `${process.env.HOME}/.vimManager` } = process.env;
const { addLineRepository } = require("../src/fileOperator");
const { clone, pull } = require("../src/systemCall");

const repositoryFullPath = (repositoryName) =>
  `${PATH_REPOSITORY}/${repositoryName}`;
const repositoryFolderName = (repositoryUrl) =>
  repositoryUrl.replace(/https?:\/\/(www\.)?/, "").replace(/\//g, "-");

const changeRepositoy = async (repositoryUrl) => {
  const repositoryName = repositoryFolderName(repositoryUrl);
  if (!fs.existsSync(repositoryFullPath(repositoryName))) {
    throw new Error("Repository no exists");
  } else {
    return addLineRepository(repositoryName);
  }
};
const addRepository = async (repositoryUrl) => {
  const repositoryName = repositoryFolderName(repositoryUrl);
  if (!fs.existsSync(repositoryFullPath(repositoryName))) {
    await clone(repositoryUrl, repositoryName);
  }
  return Promise.resolve(repositoryUrl);
};

const removeRepository = async (repositoryName) => {
  const foldedRemoved = `${PATH_REPOSITORY}/${repositoryName}`;
  if (fs.existsSync(repositoryFullPath(repositoryName))) {
    await rmPromise(foldedRemoved, { recursive: true });
    return Promise.resolve({ repositoryName, foldedRemoved });
  } else {
    throw new Error("Repository no exists");
  }
};

const updateRepository = async () => {
  const repositoriesFolder = await readdirPromise(PATH_REPOSITORY);
  return await Promise.all(
    repositoriesFolder.map((repositoryName) =>
      pull(repositoryName).then(() => {
        return `${repositoryName} updated`;
      })
    )
  );
};

const listRepository = async () => readdirPromise(PATH_REPOSITORY);

module.exports = {
  addRepository,
  updateRepository,
  listRepository,
  removeRepository,
  changeRepositoy,
};
