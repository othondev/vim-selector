const { PATH_REPOSITORY = `${process.env.HOME}/.vimManager` } = process.env;
const fs = require("fs");

exports.removeFolder = () => {
  if (fs.existsSync(PATH_REPOSITORY)) {
    fs.rmdirSync(PATH_REPOSITORY, { recursive: true });
  }
};

exports.createFolder = () => {
  if (!fs.existsSync(PATH_REPOSITORY)) {
    fs.mkdirSync(PATH_REPOSITORY, { recursive: true });
  }
};
