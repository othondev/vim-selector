//TODO mock the git clone to avoid go to internet to clone object.
//TODO test cli bin.
const { expect } = require("chai");
const {
  addRepository,
  removeRepository,
  listRepository,
  changeRepositoy,
  updateRepository,
} = require("../src/vimManager");
const { createFolder, removeFolder } = require("../src/folderOperator");

const fs = require("fs");
const { PATH_VIMRC, PATH_REPOSITORY, NODE_ENV } = process.env;

const testObjects = [
  {
    repositoryLink: "https://github.com/nvie/vimrc",
    repositoryNameExpected: "github.com-nvie-vimrc",
  },
  {
    repositoryLink: "https://github.com/vgod/vimrc",
    repositoryNameExpected: "github.com-vgod-vimrc",
  },
];

describe("command test suites", async () => {
  beforeEach("Create repository folder", () => {
    createFolder();
  });

  it("Should be loaded environment variable", () => {
    expect(NODE_ENV).equal("test");
    expect(PATH_REPOSITORY).equal("/tmp/vimManager");
  });

  it("Should clone repository", async () => {
    const added = await addRepository(testObjects[0].repositoryLink);
    expect(added).equals("https://github.com/nvie/vimrc");
  });

  it("Should remove repository", async () => {
    await addRepository(testObjects[0].repositoryLink);
    const removed = await removeRepository(
      testObjects[0].repositoryNameExpected
    );
    expect(removed.repositoryName).equal(testObjects[0].repositoryNameExpected);
    expect(removed.foldedRemoved).equal(
      `/tmp/vimManager/${testObjects[0].repositoryNameExpected}`
    );
  });

  it("Should list repository", async () => {
    await addRepository(testObjects[0].repositoryLink);
    await addRepository(testObjects[1].repositoryLink);

    const addedRepository = await listRepository();
    expect(addedRepository)
      .to.be.an("array")
      .to.have.lengthOf(2)
      .that.does.include(testObjects[0].repositoryNameExpected)
      .that.does.include(testObjects[1].repositoryNameExpected);
  });

  it("Should update repositories", async () => {
    await addRepository(testObjects[0].repositoryLink);
    await addRepository(testObjects[1].repositoryLink);
    const updateResult = await updateRepository();

    expect(updateResult)
      .to.be.an("array")
      .to.have.lengthOf(2)
      .to.have.members([
        "github.com-nvie-vimrc updated",
        "github.com-vgod-vimrc updated",
      ]);
  });
  it("Should be change the line that init the vimrc repository", async () => {
    fs.copyFileSync(`${__dirname}/fixtures/vimrc_with_repo`, `${PATH_VIMRC}`);
    await addRepository(testObjects[0].repositoryLink);
    await changeRepositoy(testObjects[0].repositoryNameExpected);

    const fileText = fs.readFileSync(PATH_VIMRC).toString("utf-8");
    expect(fileText.split("\n")).contains(
      `source ${PATH_REPOSITORY}/${testObjects[0].repositoryNameExpected}/vimrc`
    );

    fs.unlinkSync(PATH_VIMRC);
  });

  afterEach("Remove repository folder", () => {
    removeFolder();
  });
});
