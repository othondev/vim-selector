#!/bin/bash
repo='https://github.com/nvie/vimrc'
repo_path='/root/.vimManager/github.com-nvie-vimrc'

chvimrc $repo
[ ! -d "$repo_path" ] && exit 1

[ "$(chvimrc)" == '[]' ] && exit 2

chvimrc remove github.com-nvie-vimrc
[ -d "$repo_path" ] && exit 3

exit 0
