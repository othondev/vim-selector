const { expect } = require("chai");
const sinon = require('sinon')

const vimManager = require("../src/vimManager");
const selectCommand = require("../src/selectCommand");

describe("commands tests", () => {
  let addStubed
  let chStubed;
  let lsStubed;
  let upStubed;
  beforeEach(() => {
    addStubed = sinon.stub(vimManager, "addRepository").returns(Promise.resolve())
    chStubed = sinon.stub(vimManager, "changeRepositoy")
    lsStubed = sinon.stub(vimManager, "listRepository")
    upStubed = sinon.stub(vimManager, "updateRepository")
    rmStubed = sinon.stub(vimManager, "removeRepository")
  });
  afterEach(() => {
    sinon.restore();
  });

  it("Should be call add function", async () => {
    await selectCommand(["www.repository-test.test"]);
    expect(addStubed.calledOnce).true;
    expect(chStubed.calledOnce).true;
  });

  it("Should be call list function", async () => {
    await selectCommand([""]);
    expect(lsStubed.calledOnce).true;
  })

  it("Should be call update function", async () => {
    await selectCommand(["update"]);
    expect(upStubed.calledOnce).true;
  })

  it("Should be call remove function", async () => {
    await selectCommand(["remove"]);
    expect(rmStubed.calledOnce).true;
  })

});
