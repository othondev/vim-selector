const fs = require("fs-extra");
const { changeRepositoy, listRepository } = require("../src/vimManager");
const { addLineRepository } = require("../src/fileOperator");
const { PATH_VIMRC, PATH_REPOSITORY } = process.env;
const { expect } = require("chai");

const cp = (repoName) =>
  fs.copySync(
    `${__dirname}/fixtures/${repoName}`,
    `${PATH_REPOSITORY}/${repoName}`
  );

describe("file test suites", () => {

  it("Should add twice in end of line in no vimrc created", async () => {
    cp('repotestEmpty')
    await addLineRepository("repotestEmpty");
    const firstRead = fs.readFileSync(PATH_VIMRC);
    expect(firstRead.toString()).equals(
      "source /tmp/vimManager/repotestEmpty/vimrc"
    );

    cp('repotest2')
    await addLineRepository("repotest2");
    const secondRead = fs.readFileSync(PATH_VIMRC);
    expect(secondRead.toString()).equals(
      "source /tmp/vimManager/repotest2/vimrc"
    );
  });

  it("Should add in end of line in vimrc created", async () => {
    fs.copyFileSync(`${__dirname}/fixtures/vimrc`, `${PATH_VIMRC}`);
    cp('repotest')
    await addLineRepository("repotest");
    const file = fs.readFileSync(PATH_VIMRC);
    const lines = file.toString().split("\n");
    const index = lines.findIndex(
      (l) => l === "source /tmp/vimManager/repotest/vimrc"
    );

    expect(index).equals(lines.length - 1);
  });

  it("Should find vimrc file in the folder", async () => {
    cp('vimsource')
    const list = await listRepository();
    expect(list).length(1).contains("vimsource");

    await changeRepositoy("vimsource");
    const vimrcFile = fs.readFileSync(PATH_VIMRC).toString("utf-8");
    expect(vimrcFile).equals(
      "source /tmp/vimManager/vimsource/src/hidden/vimrc"
    );
  });

  it("Should throw error no vimrc file in the folder", async () => {
    cp('vimsource-novimrc')
    const list = await listRepository();
    expect(list).length(1).contains("vimsource-novimrc");

    try {
      await changeRepositoy("vimsource");
    } catch (err) {
      expect(err).to.be.a("Error");
    }
  });

  afterEach(() => {
    if (fs.existsSync(PATH_VIMRC)) fs.removeSync(PATH_VIMRC);

    if (fs.pathExistsSync(PATH_REPOSITORY))
      fs.rmdirSync(PATH_REPOSITORY, { recursive: true });
  });
});
